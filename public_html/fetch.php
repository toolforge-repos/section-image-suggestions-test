<?php

$config = array_merge(
	parse_ini_file( __DIR__.'/../config.ini', true ),
	file_exists(__DIR__ . '/../replica.my.cnf') ? parse_ini_file( __DIR__ . '/../replica.my.cnf', true ) : []
);
$mysqli = new mysqli( $config['db']['host'], $config['client']['user'],
	$config['client']['password'], $config['db']['dbname'] );
if ( $mysqli->connect_error ) {
	die('Connect Error (' . $mysqli->connect_errno . ') '
	    . $mysqli->connect_error);
}

try {
    $langCode = ( $_GET[ 'langCode' ] ?? 'en' ) ;
    $query = 'select id, wiki_db, target_title, target_section_heading, suggested_image, topic_qid,
        origin_wikis from ratedSuggestions
        where wiki_db="'. $mysqli->real_escape_string( $langCode . 'wiki' ) .'" and suggested_image like "%.jpg" ';
    if ( ( $_GET['title'] ?? false ) && ( $_GET['section'] ?? false ) ) {
        $query .= 'and target_title="' .$mysqli->real_escape_string( $_GET['title'] ) .
        '" and target_section_heading="' . $mysqli->real_escape_string( $_GET['section'] ) . '"';
    } else {
        $query .= 'and rating is null order by suggestion_origin="intersection" desc, rand()
        limit 1';
    }
    $result = $mysqli->query( $query );

    if ( $result === false ) {
        throw new \Exception( 'No suggestions found' );
    }
    $data = $result->fetch_assoc();
    if ( !$data ) {
        throw new \Exception( 'No suggestions found' );
    }
    $data['langCode'] = $langCode;
    $data['suggested_image'] = 'File:' . $data['suggested_image'];
    $data += fetchExtraData( $data );

    if ( empty( $data['section'] ) ) {
        $mysqli->query( 'update ratedSuggestions set rating=-3 where id=' .intval( $data['id'] ) );

        throw new \Exception( 'Section ' . trim( $data['target_section_heading'] ) .
            ' not found in article ' . $data['target_title'] );
    }
    if ( is_null( $data['thumbnail'] ) ) {
        $mysqli->query( 'update ratedSuggestions set rating=-3 where id=' .intval( $data['id'] ) );

        throw new \Exception( 'Thumbnail not found for ' . $data['suggested_image'] );
    }
    header( 'Content-Type: application/json' );
    echo json_encode( $data );
} catch ( \Exception $e ) {
    header( $_SERVER['SERVER_PROTOCOL'] . ' 500 Internal Server Error', true, 500 );
    header( 'Content-Type: application/json' );
    echo json_encode( [ "error" => $e->getMessage() ] );
}

$mysqli->close();

function fetchExtraData( array $data ) : array {
    // fetch the extra data in parallel to save time
    $curlHandles = [
        'thumbnail' => curl_init(
            'https://commons.wikimedia.org/w/api.php?action=query&format=json&prop=imageinfo&titles=' .
            urlencode( $data['suggested_image'] ) . '&iiprop=url'
        ),
        'section' => curl_init(
            'https://' . $data['langCode'] . '.wikipedia.org/api/rest_v1/page/mobile-sections/' .
            urlencode( $data['target_title'] )
        )
    ];

    if ( $data['topic_qid'] ) {
        $curlHandles['wikidata'] = curl_init(
            'https://www.wikidata.org/wiki/Special:EntityData/' . $data['topic_qid'] . '.json'
        );
    }
    $mh = curl_multi_init();
    foreach ( $curlHandles as $curlHandle ) {
        curl_setopt( $curlHandle, CURLOPT_RETURNTRANSFER, true );
        curl_multi_add_handle($mh, $curlHandle);
    }

    //fetch the data
    do {
        $status = curl_multi_exec( $mh, $active );
        if ( $active ) {
            curl_multi_select( $mh );
        }
    } while ( $active && $status === CURLM_OK );
    if ( $status != CURLM_OK ) {
        throw new Exception( 'Curl error: ' . curl_multi_strerror( $status ) );
    }
    //close the handles
    foreach ( $curlHandles as $curlHandle ) {
        curl_multi_remove_handle($mh, $curlHandle);
    }
    curl_multi_close($mh);

    return [
        'thumbnail' => getThumbnail( $data, curl_multi_getcontent( $curlHandles['thumbnail'] ) ),
        'section' => getSection( $data, curl_multi_getcontent( $curlHandles['section'] ) ),
        'suggestion_reason' => getSuggestionReason(
            $data,
            isset( $curlHandles['wikidata'] ) ? curl_multi_getcontent( $curlHandles['wikidata'] ) : null
        )
    ];
}


function getThumbnail( array $data, string $responseBody ) : ?string {
    $result = json_decode( $responseBody, true );
    if ( is_null( $result ) ) {
        throw new Exception( "Unexpected result format for thumbnail query for " .
            $data['suggested_image'] ." " . json_last_error() );
    }

    $page = array_shift($result['query']['pages']);
    $url = $page['imageinfo'][0]['url'] ?? null;
    if (!$url) {
        return null;
    }

    $src = str_replace( '/commons/', '/commons/thumb/', $url ) . '/';
    if ( substr( $url, strrpos( $url, '.' ) + 1 ) == 'tif' ) {
        $src .= 'lossy-page1-800px-thumbnail.tif.jpg';
        return $src;
    }
    $src .= '800px-' . substr( $url, strrpos( $url, '/' ) + 1 );
    if ( substr( $url, strrpos( $url, '.' ) + 1 ) == 'svg' ) {
        $src .= '.png';
    }
    return urldecode( $src );
}

function getSection( array $data, string $responseBody ) : string {
    $result = json_decode( $responseBody, true );
    if ( is_null( $result ) ) {
        throw new Exception( "Unexpected result format for section query." );
    }

    $sectionStartFound = $sectionEndFound = false;
    $sectionText = '';
    foreach ( $result['remaining']['sections'] as $index => $section ) {
        if (
            !$sectionStartFound
            && $section['toclevel'] == 1
            && mb_strtolower( trim( $section['line'] ) ) == mb_strtolower( trim( $data['target_section_heading'] ) )
        ) {
            $sectionStartFound = true;
            $sectionText .= '<h2>' . $section['line'] . '</h2>';
            $sectionText .= cleanSectionText( $section["text"], $data['langCode'] );
        } elseif (
            $sectionStartFound
            && !$sectionEndFound
        ) {
            if ( $section['toclevel'] == 1 ) {
                $sectionEndFound = true;
            } else {
                $level = $section['toclevel'] + 1;
                $sectionText .= '<h' . $level . '>' . $section['line'] . '</h' . $level . '>';
                $sectionText .= cleanSectionText( $section["text"], $data['langCode'] );
            }
        }
    }
    return $sectionText;
}

function cleanSectionText( string $sectionText, string $langCode ) : string {
    // make relative links into absolute links with target="_blank"
    return preg_replace(
        '/href="(\/wiki\/[^"]*)"/i',
        'target="_blank" href="https://'. $langCode . '.wikipedia.org\\1"',
        trim( $sectionText )
    );
}

function getTopic( string $wikidataResponseString, string $langCode, string $topicId ) : string {
    $result = json_decode( $wikidataResponseString, true );
    if ( is_null( $result ) ) {
        throw new Exception( "Unexpected result format for wikidata query." );
    }
    $topic = $result['entities'][$topicId]['labels'][$langCode]['value'] ?? '';
    if (
        $langCode != 'en'
        && isset( $result['entities'][$topicId]['labels']['en']['value'] )
        && $result['entities'][$topicId]['labels']['en']['value'] != $topic
    ) {
        $topic .= ' (' . $result['entities'][$topicId]['labels']['en']['value'] . ')';
    }
    return $topic;
}

function getSuggestionReason( array $data, ?string $wikidataResponseString ) : string {
    $reason = [];
    if ( !is_null( $wikidataResponseString ) ) {
        $topic = getTopic( $wikidataResponseString, $data['langCode'], $data['topic_qid'] );
        $reason[] = 'This is the image for the wikidata item <a target="_blank" ' .
            'href="https://www.wikidata.org/wiki/' . strtoupper( $data['topic_qid'] ) . '">' . $topic . '</a>' .
            ', and an article about that item is linked from the section.';
    }
    if ( $data['origin_wikis'] ) {
        $reason[] = 'The image is used for the equivalent article section in ' . $data['origin_wikis'] . '.';
    }
    if ( count( $reason ) > 1 ) {
        return '<ul><li>'.
            implode( '</li><li>', $reason ) .
            '</li></ul>';
    } else {
        return $reason[0];
    }
}
