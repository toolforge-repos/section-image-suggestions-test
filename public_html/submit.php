<?php

$config = parse_ini_file( __DIR__ . '/../config.ini', true );
if ( file_exists( __DIR__ . '/../replica.my.cnf' ) ) {
    $config = array_merge(
        $config,
        parse_ini_file( __DIR__ . '/../replica.my.cnf', true )
    );
}
$mysqli = new mysqli( $config['db']['host'], $config['client']['user'],
    $config['client']['password'], $config['db']['dbname'] );
if ( $mysqli->connect_error ) {
    die('Connect Error (' . $mysqli->connect_errno . ') '
        . $mysqli->connect_error);
}

if ( !isset( $_POST['id'] ) || !isset( $_POST['rating'] ) ) {
    throw new Exception( 'Missing data' );
}

echo "rated section image suggestions " . intval( $_POST['id'] ) . " with " . intval( $_POST['rating'] ) . "\n";
$mysqli->query(
    'update ratedSuggestions '.
    ' set rating='. intval( $_POST['rating'] ) .
    ' where id=' . intval( $_POST['id'] )
);

$mysqli->close();
