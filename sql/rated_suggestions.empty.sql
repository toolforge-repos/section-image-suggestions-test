drop table if exists ratedSuggestions;

create table ratedSuggestions (
    id int not null auto_increment,
    suggestion_origin varchar(255) not null,
    wiki_db varchar(10) not null,
    target_qid varchar(10) not null,
    target_title varchar(255) not null,
    target_section_heading varchar(255) not null,
    suggested_image varchar(255) not null,
    topic_qid varchar(10) default null,
    topic_score float default null,
    origin_wikis varchar(255) default null,
    rating tinyint default null,
    ts datetime default null,
    primary key (id)
) engine=innodb;

alter table ratedSuggestions modify ts datetime default null on update CURRENT_TIMESTAMP;